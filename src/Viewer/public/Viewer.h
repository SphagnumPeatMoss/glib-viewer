#pragma once
#include "forward.h"
#include "Resolution.h"

//////////////////////////////////////////////////////////////////////////////////////////////
// Description:																				//
//		Viewer encapsulates initialization and manipulation of the application window and	//
//		graphics API. After initialization the Viewer, you may write your own Direct3D9		//
//		drawing instructions.																//
//																							//
// Limits:																					//
//		This class is a Singleton.															//
//																							//
// Usage:																					//
//		1. Create a Params object for the Viewer and fill out required members.				//
//		2. Create the Viewer with the Params object, and store its singleton instance in	//
//		   the object you wish to manage its lifetime with.	This will initialize Direct3D9	//
//		   and spawn the application window.												//
//		3. Call BeginScene() before your drawing instructions.								//
//		4. Call EndScene() after your drawing instructions.									//
//////////////////////////////////////////////////////////////////////////////////////////////

namespace gas
{
	namespace Params
	{
		struct GOLDEN_API Viewer
		{
			// Required
			HINSTANCE			hInstance;				// Handle to the application instance, from WinMain.
			WNDPROC				winProc;				// The Window Procedure that will process Window messages.  Download our Input library if you don't want to use your own.

			// Optional
			Resolution			clientDimensions;		// The pixel width and height of the area in which to draw. Defaults to 1024x768.
			WindowMode			windowMode;				// Whether you want the drawing area to be inside a window on your desktop or to take up the full screen.  Defaults to windowed.
			VsyncMode			vsync;					// Prevent screen tearing by synchronizing frame rate with monitor refresh rate.  Defaults to Off. Note: Vsync only works in fullscreen mode.
			AntiAliasMode		antiAlias;				// Prevent aliasing (jagged edges) by blurring samples. Defaults to None.
			real				brightness;				// Increase or decrease brightness to the overall scene. Defaults to 1 (100%).
			BackBufferFormat	backBufferFormat;		// The pixel format of your backbuffer. Defaults to X8R8G8B8 (32 bits - 8 per color channel, and 8 unused).
			DepthBufferFormat	depthBufferFormat;		// The pixel format of your depth buffer. Defaults to D24S8 (32 bits - 24 for depth, 8 for stencil).
			string				screenShotDirectory;	// The path of the directory to save screenshots to.  Defaults to execution directory /Screenshots
			real				screenShotDelay;		// How long, in seconds, to wait after taking a screenshot before allowing another screenshot. Defaults to 2 seconds. Minimum is 1 second.
			LPCTSTR				cmdLine;				// The command line for the application, excluding the program name. Defaults to NULL.
			int					cmdShow;				// How the application window is to be shown, from WinMain.  Should default to SW_SHOWDEFAULT (10).
			LPCTSTR				windowID;				// The class name that identifies your window during creation/destruction. Limit to 256 characters. Defaults to "GOLDEN_APPLICATION"
			string				windowTitle;			// The name of your application window that will appear in the window's title bar. Defaults to "GOLDEN_APPLICATION"

			// TODO: Add paths for cursor and icon images.

			Viewer()
				: hInstance(nullptr)
				, winProc(nullptr)
				, clientDimensions(Resolution::Default())
				, windowMode(WindowMode::Default)
				, vsync(VsyncMode::Default)
				, antiAlias(AntiAliasMode::Default)
				, brightness(1)
				, backBufferFormat(BackBufferFormat::Default)
				, depthBufferFormat(DepthBufferFormat::Default)
				, screenShotDirectory("Screenshots")
				, screenShotDelay(2)
				, cmdLine(nullptr)
				, cmdShow(SW_SHOWDEFAULT)
				, windowID("GOLDEN_APPLICATION")
				, windowTitle("GOLDEN_APPLICAITON")
			{
			}
		};
	}
	class GOLDEN_API Viewer
	{
		/***** Public Interface *****/
	public:
		void			BeginScene(const ColorARGB color = ColorARGB::Pink);
		void			EndScene();
		HWND			GetWindowHandle();
		WindowMode		GetWindowMode() const;
		const Resolution& GetClientDimensions() const;
		const Resolution& GetWindowDimensions() const;
		real			GetUnitHalfWidth() const;
		real			GetUnitHalfHeight() const;
		void			ChangeWindow(const int clientPixelWidth, const int clientPixelHeight, const WindowMode mode);
		void			ChangeWindow(const int clientPixelWidth, const int clientPixelHeight);
		void			ChangeWindow(const WindowMode mode);
		VsyncMode		GetVsyncMode() const;
		void			SetVsyncMode(const VsyncMode mode);
		AntiAliasMode	GetAntiAliasMode() const;
		void			SetAntiAliasMode(const AntiAliasMode mode);
		const string&	GetScreenshotDirectory() const;
		void			SetScreenshotDirectory(const string& dir);
		void			TakeScreenshot(const ImageType imgType = ImageType::Default) const;	// Note: DDS is not a supported screenshot format

		/***** Init *****/
	public:
		static uptr<Viewer>		Create(const Params::Viewer& params);

	private:
		friend struct std::default_delete<Viewer>;
		Viewer();
		~Viewer();

		/***** Data *****/
	private:
		struct Impl;
		uptr<Impl> m;

		/***** No copies *****/
	private:
		Viewer(const Viewer&) = delete;
		Viewer& operator=(const Viewer&) = delete;
	};
}