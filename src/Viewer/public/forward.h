#pragma once
#include "globals.h"

namespace gas
{
	struct Resolution;
	enum class WindowMode
	{
		Windowed,
		Fullscreen,

		Default = Windowed
	};
	enum class ColorARGB : uint
	{
		Red = 0xFFFF0000,
		MediumRed = 0xFF800000,
		Orange = 0xFFFF6600,
		Yellow = 0xFFFFFF00,
		Green = 0xFF00CC00,
		MediumGreen = 0xFF007218,
		DarkGreen = 0xFF002A09,
		Cyan = 0xFF00FFFF,
		Blue = 0xFF0000FF,
		Purple = 0xFF660066,
		Pink = 0xFFFF00FF,
		Black = 0xFF000000,
		Brown = 0xFF663300,
		White = 0xFFFFFFFF,
		Gray = 0xFF666666,

		Default = Pink
	};
	enum class ImageType
	{
		PNG,
		BMP,
		DDS,
		TGA,
		JPG,

		Default = PNG
	};
	enum class VsyncMode
	{
		Off,
		On,

		Default = Off
	};
	enum class AntiAliasMode
	{
		None,
		MSAA_2x,
		MSAA_4x,
		MSAA_6x,
		MSAA_8x,
		MSAA_12x,
		MSAA_16x,

		Default = None
	};
	enum class BackBufferFormat
	{
		None,

		// 16 bit
		R5G6B5,
		A1R5G5B5,
		A4R4G4B4,

		// 24 bit
		R8G8B8,

		// 32 bit
		X8R8G8B8,
		A8R8G8B8,
		A2R10G10B10,

		Default = X8R8G8B8,
	};
	enum class DepthBufferFormat
	{
		None,

		// 16 bit
		D16,
		D15S1,

		// 32 bit
		D24X8,
		D24S8,
		D32,

		Default = D24S8,
	};
}