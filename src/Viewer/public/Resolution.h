#pragma once
#include "forward.h"

namespace gas
{
	struct GOLDEN_API Resolution
	{
		int		width;
		int		height;

		static Resolution Default();
		Resolution(const int w, const int h);
		bool operator==(const Resolution& rhs) const;
		bool operator!=(const Resolution& rhs) const;
		bool operator<(const Resolution& rhs) const;
	};
}