#pragma once
#include "forward.h"
#include <d3d9.h>
#include <d3dx9.h>

namespace gas
{
	// Backbuffer formats
	GOLDEN_API D3DFORMAT ToD3D9(const BackBufferFormat format);
	GOLDEN_API BackBufferFormat ToGasBB(const D3DFORMAT format);

	// Depth buffer formats
	GOLDEN_API D3DFORMAT ToD3D9(const DepthBufferFormat format);
	GOLDEN_API DepthBufferFormat ToGasDB(const D3DFORMAT format);

	// Anti-alias modes
	GOLDEN_API D3DMULTISAMPLE_TYPE ToD3D9(const AntiAliasMode mode);
	GOLDEN_API AntiAliasMode ToGasAA(const D3DMULTISAMPLE_TYPE mode);
}