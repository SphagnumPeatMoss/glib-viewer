#include "AntiAliasManager.h"

#include "DeviceManager.h"
#include "helpers.h"
#include "WindowManager.h"

namespace gas
{
	struct AntiAliasManager::Impl
	{
		static bool s_bInstantiated;

		AntiAliasMode			mode;
		map<D3DMULTISAMPLE_TYPE, DWORD>	supportedModes;
		DeviceManager*			pDeviceManager;
		WindowManager*			pWindowManager;

		Impl()
			: mode(AntiAliasMode::Default)
			, pDeviceManager(nullptr)
			, pWindowManager(nullptr)
		{
			assert(!s_bInstantiated && "AntiAliasManager::Initialize -- Attempted to create more than one AntiAliasManager instance.");
			s_bInstantiated = true;
		}
		~Impl()
		{
			s_bInstantiated = false;
		}

		void DiscoverModes()
		{
			HRESULT hr;
			IDirect3D9* pObject = pDeviceManager->GetObject();

			supportedModes.insert(pair<D3DMULTISAMPLE_TYPE, DWORD>(D3DMULTISAMPLE_NONE, 0));

			const int msaaTypeCount = 6;
			D3DMULTISAMPLE_TYPE msaaTypes[msaaTypeCount] =
			{
				D3DMULTISAMPLE_2_SAMPLES,
				D3DMULTISAMPLE_4_SAMPLES,
				D3DMULTISAMPLE_6_SAMPLES,
				D3DMULTISAMPLE_8_SAMPLES,
				D3DMULTISAMPLE_12_SAMPLES,
				D3DMULTISAMPLE_16_SAMPLES
			};
			DWORD msaaQuality = 0;

			for (int i = 0; i < msaaTypeCount; ++i)
			{
				// Test for support on backbuffer format
				hr = pObject->CheckDeviceMultiSampleType(
					D3DADAPTER_DEFAULT,
					D3DDEVTYPE_HAL,
					pDeviceManager->GetParams().BackBufferFormat,
					WindowMode::Windowed == pWindowManager->GetWindowMode(),
					msaaTypes[i],
					&msaaQuality
					);
				if (D3D_OK == hr)
				{
					// Test for support on depth buffer format
					hr = pObject->CheckDeviceMultiSampleType(
						D3DADAPTER_DEFAULT,
						D3DDEVTYPE_HAL,
						pDeviceManager->GetParams().AutoDepthStencilFormat,
						WindowMode::Windowed == pWindowManager->GetWindowMode(),
						msaaTypes[i],
						&msaaQuality
						);

					if (D3D_OK == hr)
					{
						msaaQuality -= 1;
						supportedModes.insert(pair<D3DMULTISAMPLE_TYPE, DWORD>(msaaTypes[i], msaaQuality));
					}
				}
			}
		}
	};
	bool AntiAliasManager::Impl::s_bInstantiated = false;

	AntiAliasMode AntiAliasManager::GetAntiAliasMode() const
	{
		return m->mode;
	}
	void AntiAliasManager::SetAntiAliasMode(const AntiAliasMode mode)
	{
		if (mode != m->mode)
		{
			auto it = m->supportedModes.find(ToD3D9(mode));
			if (it != m->supportedModes.end())
			{
				m->mode = mode;

				D3DPRESENT_PARAMETERS& presentParams = m->pDeviceManager->GetParams();
				presentParams.MultiSampleType = it->first;
				presentParams.MultiSampleQuality = it->second;
			}
		}
	}

	bool AntiAliasManager::Initialize(const AntiAliasMode mode, DeviceManager* pDeviceManager, WindowManager* pWindowManager)
	{
		bool bSuccess = false;

		if (pDeviceManager && pWindowManager)
		{
			m->mode = mode;
			m->pDeviceManager = pDeviceManager;
			m->pWindowManager = pWindowManager;
			m->DiscoverModes();

			auto it = m->supportedModes.find(ToD3D9(mode));
			assert(it != m->supportedModes.end() && "AntiAliasManager::Initialize -- Unsupported anti-alias mode specified. Manager will be initialized with anti-aliasing disabled.");
			if (it != m->supportedModes.end())
			{
				D3DPRESENT_PARAMETERS& presentParams = pDeviceManager->GetParams();
				presentParams.MultiSampleType = it->first;
				presentParams.MultiSampleQuality = it->second;
			}

			bSuccess = true;
		}

		return bSuccess;
	}
	AntiAliasManager::AntiAliasManager()
		: m(make_unique<Impl>())
	{
	}
	AntiAliasManager::~AntiAliasManager()
	{
	}
}