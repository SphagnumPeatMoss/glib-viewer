#pragma once
#include "forward.h"

namespace gas
{
	class DeviceManager;
	class WindowManager;
	class AntiAliasManager
	{
		/***** Public Interface *****/
	public:
		AntiAliasMode	GetAntiAliasMode() const;
		void			SetAntiAliasMode(const AntiAliasMode mode);

		/***** Init *****/
	public:
		bool Initialize(const AntiAliasMode mode, DeviceManager* pDeviceManager, WindowManager* pWindowManager);
		AntiAliasManager();
		~AntiAliasManager();

		/***** Data *****/
	private:
		struct Impl;
		uptr<Impl> m;

		/***** No copies *****/
	private:
		AntiAliasManager(const AntiAliasManager&) = delete;
		AntiAliasManager& operator=(const AntiAliasManager&) = delete;
	};
}