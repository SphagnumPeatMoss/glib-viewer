#include "helpers.h"

namespace gas
{
	// Backbuffer formats
	GOLDEN_API D3DFORMAT ToD3D9(const BackBufferFormat format)
	{
		D3DFORMAT output = D3DFMT_UNKNOWN;

		switch (format)
		{
		case BackBufferFormat::R5G6B5: { output = D3DFMT_R5G6B5; break; }
		case BackBufferFormat::A1R5G5B5: { output = D3DFMT_A1R5G5B5; break; }
		case BackBufferFormat::A4R4G4B4: { output = D3DFMT_A4R4G4B4; break; }
		case BackBufferFormat::R8G8B8: { output = D3DFMT_R8G8B8; break; }
		case BackBufferFormat::X8R8G8B8: { output = D3DFMT_X8R8G8B8; break; }
		case BackBufferFormat::A8R8G8B8: { output = D3DFMT_A8R8G8B8; break; }
		case BackBufferFormat::A2R10G10B10: { output = D3DFMT_A2R10G10B10; break; }
		}

		return output;
	}
	GOLDEN_API BackBufferFormat ToGasBB(const D3DFORMAT format)
	{
		BackBufferFormat output = BackBufferFormat::None;

		switch (format)
		{
		case D3DFMT_R5G6B5: { output = BackBufferFormat::R5G6B5; break; }
		case D3DFMT_A1R5G5B5: { output = BackBufferFormat::A1R5G5B5; break; }
		case D3DFMT_A4R4G4B4: { output = BackBufferFormat::A4R4G4B4; break; }
		case D3DFMT_R8G8B8: { output = BackBufferFormat::R8G8B8; break; }
		case D3DFMT_X8R8G8B8: { output = BackBufferFormat::X8R8G8B8; break; }
		case D3DFMT_A8R8G8B8: { output = BackBufferFormat::A8R8G8B8; break; }
		case D3DFMT_A2R10G10B10: { output = BackBufferFormat::A2R10G10B10; break; }
		}

		return output;
	}

	// Depth buffer formats
	GOLDEN_API D3DFORMAT ToD3D9(const DepthBufferFormat format)
	{
		D3DFORMAT output = D3DFMT_UNKNOWN;

		switch (format)
		{
		case DepthBufferFormat::D16: { output = D3DFMT_D16; break; }
		case DepthBufferFormat::D15S1: { output = D3DFMT_D15S1; break; }
		case DepthBufferFormat::D24X8: { output = D3DFMT_D24X8; break; }
		case DepthBufferFormat::D24S8: { output = D3DFMT_D24S8; break; }
		case DepthBufferFormat::D32: { output = D3DFMT_D32; break; }
		}

		return output;
	}
	GOLDEN_API DepthBufferFormat ToGasDB(const D3DFORMAT format)
	{
		DepthBufferFormat output = DepthBufferFormat::None;

		switch (format)
		{
		case D3DFMT_D16: { output = DepthBufferFormat::D16; break; }
		case D3DFMT_D15S1: { output = DepthBufferFormat::D15S1; break; }
		case D3DFMT_D24X8: { output = DepthBufferFormat::D24X8; break; }
		case D3DFMT_D24S8: { output = DepthBufferFormat::D24S8; break; }
		case D3DFMT_D32: { output = DepthBufferFormat::D32; break; }
		}

		return output;
	}

	GOLDEN_API D3DMULTISAMPLE_TYPE ToD3D9(const AntiAliasMode mode)
	{
		D3DMULTISAMPLE_TYPE output = D3DMULTISAMPLE_NONE;

		switch (mode)
		{
		case AntiAliasMode::MSAA_2x: { output = D3DMULTISAMPLE_2_SAMPLES; break; }
		case AntiAliasMode::MSAA_4x: { output = D3DMULTISAMPLE_4_SAMPLES; break; }
		case AntiAliasMode::MSAA_6x: { output = D3DMULTISAMPLE_6_SAMPLES; break; }
		case AntiAliasMode::MSAA_8x: { output = D3DMULTISAMPLE_8_SAMPLES; break; }
		case AntiAliasMode::MSAA_12x: { output = D3DMULTISAMPLE_12_SAMPLES; break; }
		case AntiAliasMode::MSAA_16x: { output = D3DMULTISAMPLE_16_SAMPLES; break; }
		}

		return output;
	}
	GOLDEN_API AntiAliasMode ToGasAA(D3DMULTISAMPLE_TYPE mode)
	{
		AntiAliasMode output = AntiAliasMode::None;

		switch (mode)
		{
		case D3DMULTISAMPLE_2_SAMPLES: { output = AntiAliasMode::MSAA_2x; break; }
		case D3DMULTISAMPLE_4_SAMPLES: { output = AntiAliasMode::MSAA_4x; break; }
		case D3DMULTISAMPLE_6_SAMPLES: { output = AntiAliasMode::MSAA_6x; break; }
		case D3DMULTISAMPLE_8_SAMPLES: { output = AntiAliasMode::MSAA_8x; break; }
		case D3DMULTISAMPLE_12_SAMPLES: { output = AntiAliasMode::MSAA_12x; break; }
		case D3DMULTISAMPLE_16_SAMPLES: { output = AntiAliasMode::MSAA_16x; break; }
		}

		return output;
	}
}
