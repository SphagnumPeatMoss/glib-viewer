#include "VsyncManager.h"

#include "DeviceManager.h"

namespace gas
{
	struct VsyncManager::Impl
	{
		static bool s_bInstantiated;

		VsyncMode		mode;
		DeviceManager*	pDeviceManager;

		Impl()
			: mode(VsyncMode::Default)
			, pDeviceManager(nullptr)
		{
			assert(!s_bInstantiated && "VsyncManager::Initialize -- Attempted to create more than one VsyncManager instance.");
			s_bInstantiated = true;
		}
		~Impl()
		{
			s_bInstantiated = false;
		}

		void CommitChanges()
		{
			// Adjust the presentation parameters
			D3DPRESENT_PARAMETERS& presentParams = pDeviceManager->GetParams();
			if (VsyncMode::Off == mode)
			{
				presentParams.PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE;
				presentParams.FullScreen_RefreshRateInHz = 0;
			}
			else
			{
				presentParams.PresentationInterval = D3DPRESENT_INTERVAL_ONE;
				presentParams.FullScreen_RefreshRateInHz = D3DPRESENT_RATE_DEFAULT;
			}

			pDeviceManager->ResetDevice();
		}
	};
	bool VsyncManager::Impl::s_bInstantiated = false;

	VsyncMode VsyncManager::GetVsyncMode() const
	{
		return m->mode;
	}
	void VsyncManager::SetVsyncMode(const VsyncMode mode)
	{
		if (mode != m->mode)
		{
			m->mode = mode;
			m->CommitChanges();
		}
	}

	bool VsyncManager::Initialize(const VsyncMode mode, DeviceManager* pDeviceManager)
	{
		bool bSuccess = false;

		if (pDeviceManager)
		{
			m->mode = mode;
			m->pDeviceManager = pDeviceManager;

			D3DPRESENT_PARAMETERS& presentParams = pDeviceManager->GetParams();
			if (VsyncMode::Off == mode)
			{
				presentParams.PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE;
				presentParams.FullScreen_RefreshRateInHz = 0;
			}
			else
			{
				presentParams.PresentationInterval = D3DPRESENT_INTERVAL_ONE;
				presentParams.FullScreen_RefreshRateInHz = D3DPRESENT_RATE_DEFAULT;
			}

			bSuccess = true;
		}

		return bSuccess;
	}
	VsyncManager::VsyncManager()
		: m(make_unique<Impl>())
	{
	}
	VsyncManager::~VsyncManager()
	{
	}
}