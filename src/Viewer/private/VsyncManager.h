#pragma once
#include "forward.h"

namespace gas
{
	class DeviceManager;
	class VsyncManager
	{
		/***** Public Interface *****/
	public:
		VsyncMode	GetVsyncMode() const;
		void		SetVsyncMode(const VsyncMode mode);

		/***** Init *****/
	public:
		bool Initialize(const VsyncMode mode, DeviceManager* pDeviceManager);
		VsyncManager();
		~VsyncManager();

		/***** Data *****/
	private:
		struct Impl;
		uptr<Impl> m;

		/***** No copies *****/
	private:
		VsyncManager(const VsyncManager&) = delete;
		VsyncManager& operator=(const VsyncManager&) = delete;
	};
}