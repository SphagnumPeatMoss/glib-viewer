#pragma once
#include "forward.h"

namespace gas
{
	namespace Params { struct Viewer; }
	class DeviceManager;
	class WindowManager
	{
		/***** Public Interface *****/
	public:
		HWND		GetWindowHandle();

		// Accessors
		WindowMode	GetWindowMode() const;
		const Resolution& GetClientDimensions() const;
		const Resolution& GetWindowDimensions() const;
		real		GetUnitHalfWidth() const;
		real		GetUnitHalfHeight() const;

		// Mutators
		void		ChangeWindow(const int clientWidth, const int clientHeight, const WindowMode mode);
		void		ChangeWindow(const int clientWidth, const int clientHeight);
		void		ChangeWindow(const WindowMode mode);

		/***** Init *****/
	public:
		bool Initialize(
			HINSTANCE hInstance
			, WNDPROC winProc
			, const Resolution& clientDimensions
			, const WindowMode winMode
			, LPCTSTR cmdLine
			, const int cmdShow
			, LPCTSTR className
			, const string& windowTitle
			, DeviceManager* pDeviceManager);
		WindowManager();
		~WindowManager();

		/***** Data *****/
	private:
		struct Impl;
		uptr<Impl> m;

		/***** No copies *****/
	private:
		WindowManager(const WindowManager&) = delete;
		WindowManager& operator=(const WindowManager&) = delete;
	};
}