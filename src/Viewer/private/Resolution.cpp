#include "Resolution.h"

namespace gas
{
	Resolution Resolution::Default()
	{
		return Resolution(1024, 768);
	}
	Resolution::Resolution(const int w, const int h)
		: width(w)
		, height(h)
	{
	}

	bool Resolution::operator==(const Resolution& rhs) const
	{
		return (width == rhs.width) && (height == rhs.height);
	}
	bool Resolution::operator!=(const Resolution& rhs) const
	{
		return !(*this == rhs);
	}
	bool Resolution::operator<(const Resolution& rhs) const
	{
		if (width != rhs.width)
			return width < rhs.width;
		return height < rhs.height;
	}
}