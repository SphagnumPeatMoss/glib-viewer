#include "WindowManager.h"

#include "DeviceManager.h"
#include "Resolution.h"

namespace gas
{
	struct WindowManager::Impl
	{
		static bool s_bInstantiated;

		static const DWORD WINDOWED_STYLE_FLAGS = (WS_VISIBLE | WS_OVERLAPPEDWINDOW);
		static const DWORD WINDOWED_SHOW_FLAGS = (SWP_SHOWWINDOW);
		static const DWORD FULLSCREEN_STYLE_FLAGS = (WS_VISIBLE | WS_POPUP);
		static const DWORD FULLSCREEN_SHOW_FLAGS = (SWP_SHOWWINDOW | SWP_FRAMECHANGED | SWP_NOACTIVATE | SWP_NOSIZE);

		HINSTANCE		hInstance;
		HWND			hWnd;
		LPCTSTR			className;

		Resolution		clientDimensions;
		Resolution		windowDimensions;
		WindowMode		windowMode;

		DeviceManager*	pDeviceManager;

		Impl()
			: hInstance(nullptr)
			, hWnd(nullptr)
			, className(nullptr)
			, clientDimensions(Resolution(0, 0))
			, windowDimensions(Resolution(0, 0))
			, windowMode(WindowMode::Default)
			, pDeviceManager(nullptr)
		{
			assert(!s_bInstantiated && "WindowManager::Initialize -- Attempted to create more than one WindowManager instance.");
			s_bInstantiated = true;
		}
		~Impl()
		{
			HRESULT hr = UnregisterClass(className, hInstance);
			DestroyWindow(hWnd);
			s_bInstantiated = false;
		}

		bool RegisterAppWindow(WNDPROC winProc)
		{
			HRESULT hr = CoInitializeEx(nullptr, COINIT_APARTMENTTHREADED | COINIT_DISABLE_OLE1DDE);

			WNDCLASSEX wc;
			ZeroMemory(&wc, sizeof(wc));

			wc.cbSize = sizeof(wc);
			wc.cbClsExtra = 0;
			wc.cbWndExtra = 0;
			wc.hbrBackground = nullptr;
			wc.hCursor = LoadCursor(nullptr, IDC_ARROW);
			wc.hIcon = LoadIcon(nullptr, IDI_APPLICATION);
			wc.hIconSm = LoadIcon(nullptr, IDI_APPLICATION);
			wc.hInstance = hInstance;
			wc.lpszMenuName = nullptr;
			wc.lpfnWndProc = winProc;
			wc.lpszClassName = className;
			wc.style = CS_DBLCLKS | CS_OWNDC | CS_HREDRAW | CS_VREDRAW;

			ATOM result = RegisterClassEx(&wc);
			assert(result && "WindowManager::Initialize -- Failed to register window class.");
			if (result)
				return true;
			return false;
		}
		bool CreateAppWindow(const string& windowTitle)
		{
			const DWORD styleFlags = WindowMode::Fullscreen == windowMode ? FULLSCREEN_STYLE_FLAGS : WINDOWED_STYLE_FLAGS;

			hWnd = CreateWindowEx(
				WS_EX_APPWINDOW,												// extended style flags
				className,														// window class
				windowTitle.c_str(),											// window title
				styleFlags | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,					// window styles
				GetSystemMetrics(SM_CXSCREEN) / 2 - windowDimensions.width / 2,	// x coord
				GetSystemMetrics(SM_CYSCREEN) / 2 - windowDimensions.height / 2,	// y coord
				windowDimensions.width,											// window width
				windowDimensions.height,										// window height
				HWND_DESKTOP,													// handle to parent window
				nullptr,														// handle to menu
				hInstance,														// handle to application
				nullptr															// creation parameters
				);
			assert(hWnd && "WindowManager::Initialize -- Failed to create the window.");
			if (hWnd)
				return true;
			return false;
		}
		void ShowAppWindow(const int cmdShow)
		{
			ShowWindow(hWnd, cmdShow);
			UpdateWindow(hWnd);
		}
		Resolution ComputeWindowDimensions(const DWORD styleFlags)
		{
			Resolution res(0, 0);

			RECT rect = { 0, 0, clientDimensions.width, clientDimensions.height };
			AdjustWindowRectEx(&rect, styleFlags, FALSE, WS_EX_APPWINDOW);
			res.width = rect.right - rect.left;
			res.height = rect.bottom - rect.top;

			return res;
		}
		void CommitChanges()
		{
			// Adjust the presentation parameters
			const bool bWindowed = WindowMode::Windowed == windowMode;
			D3DPRESENT_PARAMETERS& presentParams = pDeviceManager->GetParams();
			presentParams.Windowed = bWindowed;
			if (bWindowed)
			{
				presentParams.BackBufferWidth = clientDimensions.width;
				presentParams.BackBufferHeight = clientDimensions.height;
			}
			else
			{
				// NOTE: Goes blackscreen in X8R8G8B8 if the two lines below are commented out.
				presentParams.BackBufferWidth = GetSystemMetrics(SM_CXSCREEN);
				presentParams.BackBufferHeight = GetSystemMetrics(SM_CYSCREEN);
			}

			// Reset the device
			pDeviceManager->ResetDevice();

			// Modify window
			if (WindowMode::Fullscreen == windowMode)
			{
				// Adjust window styles
				SetWindowLong(hWnd, GWL_STYLE, FULLSCREEN_STYLE_FLAGS);

				// Adjust window
				SetWindowPos(
					hWnd,
					HWND_TOP,
					0, 0, 0, 0,
					FULLSCREEN_SHOW_FLAGS
					);
			}
			else if (WindowMode::Windowed == windowMode)
			{
				// Adjust window styles
				SetWindowLong(hWnd, GWL_STYLE, WINDOWED_STYLE_FLAGS);
				SetWindowLong(hWnd, GWL_STYLE, GetWindowLong(hWnd, GWL_STYLE) & ~WS_MAXIMIZEBOX);
				SetWindowLong(hWnd, GWL_STYLE, GetWindowLong(hWnd, GWL_STYLE) & ~WS_THICKFRAME);

				// Adjust window
				windowDimensions = ComputeWindowDimensions(WINDOWED_STYLE_FLAGS);
				SetWindowPos(
					hWnd,
					HWND_NOTOPMOST,
					GetSystemMetrics(SM_CXSCREEN) / 2 - windowDimensions.width / 2,
					GetSystemMetrics(SM_CYSCREEN) / 2 - windowDimensions.height / 2,
					windowDimensions.width,
					windowDimensions.height,
					WINDOWED_SHOW_FLAGS
					);
			}
		}
	};
	bool WindowManager::Impl::s_bInstantiated = false;

	HWND WindowManager::GetWindowHandle()
	{
		return m->hWnd;
	}

	// Accessors
	WindowMode WindowManager::GetWindowMode() const
	{
		return m->windowMode;
	}
	const Resolution& WindowManager::GetClientDimensions() const
	{
		return m->clientDimensions;
	}
	const Resolution& WindowManager::GetWindowDimensions() const
	{
		return m->windowDimensions;
	}
	real WindowManager::GetUnitHalfWidth() const
	{
		const real aspect = real(m->clientDimensions.width) / real(m->clientDimensions.height);
		return aspect;
	}
	real WindowManager::GetUnitHalfHeight() const
	{
		return real(1.0);
	}

	// Mutators
	void WindowManager::ChangeWindow(const int clientWidth, const int clientHeight, const WindowMode mode)
	{
		const Resolution res(clientWidth, clientHeight);
		if ((mode != m->windowMode) || (res != m->clientDimensions))
		{
			// Validate the new resolution
			const bool bIsValid = m->pDeviceManager->ValidateResolution(res);
			assert(bIsValid && "WindowManager::ChangeWindow -- Attempted to switch to an unsupported resoltuion.");
			if (bIsValid)
			{
				// Set the new resolution and mode
				m->clientDimensions = res;
				m->windowMode = mode;

				// Adjust the presentation parameters and window
				m->CommitChanges();
			}
		}
	}
	void WindowManager::ChangeWindow(const int clientWidth, const int clientHeight)
	{
		const Resolution res(clientWidth, clientHeight);
		if (res != m->clientDimensions)
		{
			// Validate the new resolution
			const bool bIsValid = m->pDeviceManager->ValidateResolution(res);
			assert(bIsValid && "WindowManager::ChangeWindow -- Attempted to switch to an unsupported resoltuion.");
			if (bIsValid)
			{
				// Set the new resolution
				m->clientDimensions = res;

				// Adjust the presentation parameters and window
				m->CommitChanges();
			}
		}
	}
	void WindowManager::ChangeWindow(const WindowMode mode)
	{
		if (mode != m->windowMode)
		{
			// Set the new mode
			m->windowMode = mode;

			// Adjust the presentation parameters and window
			m->CommitChanges();
		}
	}

	bool WindowManager::Initialize(
		HINSTANCE hInstance
		, WNDPROC winProc
		, const Resolution& clientDimensions
		, const WindowMode winMode
		, LPCTSTR cmdLine
		, const int cmdShow
		, LPCTSTR className
		, const string& windowTitle
		, DeviceManager* pDeviceManager)
	{
		bool bSuccess = false;

		assert(hInstance && "WindowManager::Initialize -- Invalid handle to application instance.");
		assert(winProc && "WindowManager::Initialize -- Invalid window procedure.");
		assert(className && "WindowManager::Initialize -- Invalid window class name.");
		assert(windowTitle.size() && "WindowManager::Initialize -- Invalid window title.");
		assert(pDeviceManager && "WindowManager::Initialize -- Invalid Device Manager.");
		if (hInstance && className && windowTitle.size() && pDeviceManager)
		{
			m->hInstance = hInstance;
			m->className = className;
			m->clientDimensions = clientDimensions;
			m->windowMode = winMode;
			m->pDeviceManager = pDeviceManager;

			// Register the window
			if (m->RegisterAppWindow(winProc))
			{
				const bool bWindowed = WindowMode::Windowed == winMode;

				// Create the window
				m->windowDimensions = m->ComputeWindowDimensions(bWindowed ? m->WINDOWED_STYLE_FLAGS : m->FULLSCREEN_STYLE_FLAGS);
				if (m->CreateAppWindow(windowTitle))
				{
					// Show the window
					m->ShowAppWindow(cmdShow);

					// Set the presentation parameters
					D3DPRESENT_PARAMETERS& presentParams = pDeviceManager->GetParams();
					presentParams.hDeviceWindow = m->hWnd;
					presentParams.Windowed = bWindowed;
					if (bWindowed)
					{
						presentParams.BackBufferWidth = m->clientDimensions.width;
						presentParams.BackBufferHeight = m->clientDimensions.height;
					}
					else
					{
						// NOTE: Goes blackscreen in X8R8G8B8 if the two lines below are commented out.
						presentParams.BackBufferWidth = GetSystemMetrics(SM_CXSCREEN);
						presentParams.BackBufferHeight = GetSystemMetrics(SM_CYSCREEN);
					}

					bSuccess = true;
				}
				else
				{
					m.reset();
					m = make_unique<Impl>();
				}
			}
		}

		return bSuccess;
	}
	WindowManager::WindowManager()
		: m(make_unique<Impl>())
	{
	}
	WindowManager::~WindowManager()
	{
	}
}