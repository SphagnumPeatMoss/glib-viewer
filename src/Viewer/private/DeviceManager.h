#pragma once
#include "forward.h"
#include <d3d9.h>

#undef GetObject

namespace gas
{
	namespace Params{ struct Viewer; }
	class WindowManager;
	class VsyncManager;
	class AntiAliasManager;
	class DeviceManager
	{
		/***** Public Interface *****/
	public:
		IDirect3D9*					GetObject();
		IDirect3DDevice9*			GetDevice();
		D3DPRESENT_PARAMETERS&		GetParams();
		void						ResetDevice();
		const vector<Resolution>&	GetResolutions() const;
		bool						ValidateResolution(const Resolution& res) const;

		void	BeginScene(const ColorARGB color);
		void	EndScene();

		/***** Init *****/
	public:
		bool DeviceManager::Initialize(
			const Params::Viewer& params
			, WindowManager* pWindowManager
			, VsyncManager* pVsyncManager
			, AntiAliasManager* pAntiAliasManager);
		DeviceManager();
		~DeviceManager();

		/***** Data *****/
	private:
		struct Impl;
		uptr<Impl> m;

		/***** No copies *****/
	private:
		DeviceManager(const DeviceManager&) = delete;
		DeviceManager& operator=(const DeviceManager&) = delete;
	};
}