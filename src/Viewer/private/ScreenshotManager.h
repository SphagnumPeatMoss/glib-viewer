#pragma once
#include "forward.h"

namespace gas
{
	class DeviceManager;
	class WindowManager;
	class ScreenshotManager
	{
		/***** Public Interface *****/
	public:
		const string&	GetScreenshotDir() const;
		void			SetScreenshotDir(const string& dir);
		void			TakeScreenshot(const ImageType imgType = ImageType::Default);

		/***** Init *****/
	public:
		bool Initialize(const string& dir, const real delay, DeviceManager* pDeviceManager, WindowManager* pWindowManager);
		ScreenshotManager();
		~ScreenshotManager();

		/***** Data *****/
	private:
		struct Impl;
		uptr<Impl> m;

		/***** No copies *****/
	private:
		ScreenshotManager(const ScreenshotManager&) = delete;
		ScreenshotManager& operator=(const ScreenshotManager&) = delete;
	};
}