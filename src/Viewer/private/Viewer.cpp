#include "Viewer.h"

#include "AntiAliasManager.h"
#include "DeviceManager.h"
#include "ScreenshotManager.h"
#include "VsyncManager.h"
#include "WindowManager.h"

namespace gas
{
	struct Viewer::Impl
	{
		static bool s_bInstantiated;

		uptr<DeviceManager>		pDeviceManager;
		uptr<WindowManager>		pWindowManager;
		uptr<VsyncManager>		pVsyncManager;
		uptr<AntiAliasManager>	pAntiAliasManager;
		uptr<ScreenshotManager>	pScreenshotManager;

		Impl()
		{
			s_bInstantiated = true;
		}
		~Impl()
		{
			s_bInstantiated = false;
		}
	};
	bool Viewer::Impl::s_bInstantiated = false;

	void Viewer::BeginScene(const ColorARGB color /* = ColorARGB::Pink */)
	{
		// Clear the render target and prepare for drawing.
		m->pDeviceManager->BeginScene(color);
	}
	void Viewer::EndScene()
	{
		m->pDeviceManager->EndScene();
	}
	HWND Viewer::GetWindowHandle()
	{
		return m->pWindowManager->GetWindowHandle();
	}
	WindowMode Viewer::GetWindowMode() const
	{
		return m->pWindowManager->GetWindowMode();
	}
	const Resolution& Viewer::GetClientDimensions() const
	{
		return m->pWindowManager->GetClientDimensions();
	}
	const Resolution& Viewer::GetWindowDimensions() const
	{
		return m->pWindowManager->GetWindowDimensions();
	}
	real Viewer::GetUnitHalfWidth() const
	{
		return m->pWindowManager->GetUnitHalfWidth();
	}
	real Viewer::GetUnitHalfHeight() const
	{
		return m->pWindowManager->GetUnitHalfHeight();
	}
	void Viewer::ChangeWindow(const int clientPixelWidth, const int clientPixelHeight, const WindowMode mode)
	{
		m->pWindowManager->ChangeWindow(clientPixelWidth, clientPixelHeight, mode);
	}
	void Viewer::ChangeWindow(const int clientPixelWidth, const int clientPixelHeight)
	{
		m->pWindowManager->ChangeWindow(clientPixelWidth, clientPixelHeight);
	}
	void Viewer::ChangeWindow(const WindowMode mode)
	{
		m->pWindowManager->ChangeWindow(mode);
	}
	VsyncMode Viewer::GetVsyncMode() const
	{
		return m->pVsyncManager->GetVsyncMode();
	}
	void Viewer::SetVsyncMode(const VsyncMode mode)
	{
		m->pVsyncManager->SetVsyncMode(mode);
	}
	AntiAliasMode Viewer::GetAntiAliasMode() const
	{
		return m->pAntiAliasManager->GetAntiAliasMode();
	}
	void Viewer::SetAntiAliasMode(const AntiAliasMode mode)
	{
		m->pAntiAliasManager->SetAntiAliasMode(mode);
	}
	const string& Viewer::GetScreenshotDirectory() const
	{
		return m->pScreenshotManager->GetScreenshotDir();
	}
	void Viewer::SetScreenshotDirectory(const string& dir)
	{
		m->pScreenshotManager->SetScreenshotDir(dir);
	}
	void Viewer::TakeScreenshot(const ImageType imgType /*= ImageType::Default*/) const
	{
		m->pScreenshotManager->TakeScreenshot(imgType);
	}

	uptr<Viewer> Viewer::Create(const Params::Viewer& params)
	{
		uptr<Viewer> pViewer;
		assert(!Impl::s_bInstantiated && "Viewer::Create -- Attempted to create more than one Viewer instance.");
		if (!Impl::s_bInstantiated)
		{
			bool bGoodParams = true;
			assert((bGoodParams &= (params.hInstance != nullptr)) && "Viewer::Create -- Attempted to create a Viewer with no handle to the application instance.");

			if (bGoodParams)
			{
				pViewer.reset(new Viewer());

				pViewer->m->pDeviceManager = make_unique<DeviceManager>();
				pViewer->m->pWindowManager = make_unique<WindowManager>();
				pViewer->m->pVsyncManager = make_unique<VsyncManager>();
				pViewer->m->pAntiAliasManager = make_unique<AntiAliasManager>();
				pViewer->m->pScreenshotManager = make_unique<ScreenshotManager>();

				bool bSuccess = pViewer->m->pDeviceManager->Initialize(
					params
					, pViewer->m->pWindowManager.get()
					, pViewer->m->pVsyncManager.get()
					, pViewer->m->pAntiAliasManager.get()
					);

				if (bSuccess)
				{
					pViewer->m->pScreenshotManager->Initialize(params.screenShotDirectory, params.screenShotDelay, pViewer->m->pDeviceManager.get(), pViewer->m->pWindowManager.get());
				}
				else
				{
					assert(0 && "Viewer::Create -- Failed to initialize the Device Manager.");
					pViewer.reset();
				}
			}
		}

		return pViewer;
	}

	Viewer::Viewer()
		: m(make_unique<Impl>())
	{
	}
	Viewer::~Viewer()
	{
	}
}