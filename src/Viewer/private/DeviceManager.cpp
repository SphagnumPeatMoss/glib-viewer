#include "DeviceManager.h"

#include "AntiAliasManager.h"
#include "helpers.h"
#include "Viewer.h"
#include "VsyncManager.h"
#include "WindowManager.h"

#include <algorithm>
#include <thread>

namespace gas
{
	struct DeviceManager::Impl
	{
		static bool s_bInstantiated;

		typedef void(*ObjectDeleter)(IDirect3D9*);
		typedef void(*DeviceDeleter)(IDirect3DDevice9*);

		uptr<IDirect3D9, ObjectDeleter>			pObject;
		uptr<IDirect3DDevice9, DeviceDeleter>	pDevice;
		DWORD									vertexProcessingFlags;
		D3DPRESENT_PARAMETERS					presentParams;
		HWND									hWnd;

		vector<Resolution>						supportedResolutions;

		Impl()
			: pObject(nullptr, SafeRelease<IDirect3D9>)
			, pDevice(nullptr, SafeRelease<IDirect3DDevice9>)
			, vertexProcessingFlags(0)
			, hWnd(nullptr)
		{
			ZeroMemory(&presentParams, sizeof(presentParams));

			assert(!s_bInstantiated && "DeviceManager::Initialize -- Attempted to create more than one DeviceManager instance.");
			s_bInstantiated = true;
		}
		~Impl()
		{
			s_bInstantiated = false;
		}

		bool InitObject(D3DFORMAT bbFormat)
		{
			bool bSuccess = false;

			// Initialize the Object
			HRESULT hr;
			IDirect3D9* pD3D9 = Direct3DCreate9(D3D_SDK_VERSION);
			assert(nullptr != pD3D9 && "DeviceManager::Initialize -- Failed to get graphics system interface");
			if (pD3D9)
			{
				// Ensure our adapter supports the formats we want in windowed mode
				D3DDISPLAYMODE mode;
				pD3D9->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &mode);
				hr = pD3D9->CheckDeviceType(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, mode.Format, mode.Format, true);	// windowed
				assert(D3D_OK == hr && "DeviceManager::Initialize -- Unable to verify HAL support.");
				if (D3D_OK == hr)
				{
					// Ensure our adapter supports the formats we want in fullscreen mode
					hr = pD3D9->CheckDeviceType(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, bbFormat, bbFormat, false);	// fullscreen
					assert(D3D_OK == hr && "DeviceManager::Initialize -- Unable to verify HAL support.");
					if (D3D_OK == hr)
					{
						// Check for hardware vertex processing
						D3DCAPS9 caps;
						hr = pD3D9->GetDeviceCaps(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, &caps);

						if (caps.DevCaps & D3DDEVCAPS_HWTRANSFORMANDLIGHT)
						{
							vertexProcessingFlags |= D3DCREATE_HARDWARE_VERTEXPROCESSING;
						}
						else
						{
							vertexProcessingFlags |= D3DCREATE_SOFTWARE_VERTEXPROCESSING;
						}
						if ((caps.DevCaps & D3DDEVCAPS_PUREDEVICE) && (vertexProcessingFlags & D3DCREATE_HARDWARE_VERTEXPROCESSING))
						{
							vertexProcessingFlags |= D3DCREATE_PUREDEVICE;
						}

						// Gather all supported resolutions
						Resolution res(0, 0);
						const uint modeCount = pD3D9->GetAdapterModeCount(D3DADAPTER_DEFAULT, bbFormat);
						for (uint i = 0; i < modeCount; ++i)
						{
							pD3D9->EnumAdapterModes(D3DADAPTER_DEFAULT, bbFormat, i, &mode);
							res.width = mode.Width;
							res.height = mode.Height;
							supportedResolutions.push_back(res);
						}

						// Sort resolutions and remove duplicates
						std::sort(supportedResolutions.begin(), supportedResolutions.end());
						auto newEnd = std::unique(supportedResolutions.begin(), supportedResolutions.end());
						supportedResolutions.erase(newEnd, supportedResolutions.end());

						// Ensure we have at least one supported resolution
						assert(supportedResolutions.size() > 0 && "DeviceManager::Initialize -- No supported resolutions found.");
						if (supportedResolutions.size() > 0)
						{
							pObject.reset(pD3D9);
							bSuccess = true;
						}
					}
				}
			}

			return bSuccess;
		}
		bool InitDevice()
		{
			bool bSuccess = false;

			HRESULT hr;
			IDirect3DDevice9* pD3D9Device = nullptr;
			hr = pObject->CreateDevice(
				D3DADAPTER_DEFAULT,
				D3DDEVTYPE_HAL,
				hWnd,
				vertexProcessingFlags,
				&presentParams,
				&pD3D9Device
				);

			assert(D3D_OK == hr && "DeviceManager::Initialize -- Failed to initialize device.");
			if (D3D_OK == hr)
			{
				pDevice.reset(pD3D9Device);
				bSuccess = true;
			}

			return bSuccess;
		}
	};
	bool DeviceManager::Impl::s_bInstantiated = false;

	IDirect3D9* DeviceManager::GetObject()
	{
		return m->pObject.get();
	}
	IDirect3DDevice9* DeviceManager::GetDevice()
	{
		return m->pDevice.get();
	}
	D3DPRESENT_PARAMETERS& DeviceManager::GetParams()
	{
		return m->presentParams;
	}
	void DeviceManager::ResetDevice()
	{
		m->pDevice->Reset(&m->presentParams);
	}
	const vector<Resolution>& DeviceManager::GetResolutions() const
	{
		return m->supportedResolutions;
	}
	bool DeviceManager::ValidateResolution(const Resolution& res) const
	{
		bool bIsValid = false;

		for (uint i = 0; i < m->supportedResolutions.size(); ++i)
		{
			if (res == m->supportedResolutions[i])
			{
				bIsValid = true;
				break;
			}
		}

		return bIsValid;
	}

	void DeviceManager::BeginScene(const ColorARGB color)
	{
		m->pDevice->Clear(0, 0, D3DCLEAR_TARGET, DWORD(color), 1.0, 0);
		m->pDevice->BeginScene();
		m->pDevice->Clear(0, 0, D3DCLEAR_ZBUFFER, 0, 1.0, 0);
	}
	void DeviceManager::EndScene()
	{
		m->pDevice->EndScene();

		// If D3DERR_DEVICELOST, the fullscreen application was probably tabbed out and lost focus.
		// - Continue testing the cooperative level until D3DERR_DEVICENOTRESET is returned.
		// - When D3D_DEVICENOTRESET is returned, the application has regained focus and the 
		//   device can be reset.
		// If Present is called between BeginScene/EndScene pairs, it will return D3DERR_INVALIDCALL 
		//    unless the render target is not the current render target.
		if (D3D_OK != m->pDevice->Present(0, 0, 0, 0))
		{
			// TestCooperativeLevel will fail if called on a different thread than the one used to 
			// create the device.
			HRESULT hr;
			while (D3DERR_DEVICELOST == (hr = m->pDevice->TestCooperativeLevel()))
			{
				std::this_thread::yield();	// let other threads continue without waiting on this one
			}
			if (D3DERR_DEVICENOTRESET == hr)
			{
				ResetDevice();
			}
			else
			{
				assert(0 && "Viewer::EndScene -- Presentation failed for unknown reason.");
			}
		}
	}

	bool DeviceManager::Initialize(const Params::Viewer& params, WindowManager* pWindowManager, VsyncManager* pVsyncManager, AntiAliasManager* pAntiAliasManager)
	{
		bool bSuccess = false;

		// Initialize the D3D9 Object interface
		if (m->InitObject(ToD3D9(params.backBufferFormat)))
		{
			// Validate the desired resolution
			const bool bIsValid = ValidateResolution(params.clientDimensions);
			assert(bIsValid && "DeviceManager::Initialize -- Unsupported client dimensions.");
			if (bIsValid)
			{
				// Set the presentation parameters
				m->presentParams.SwapEffect = D3DSWAPEFFECT_DISCARD;
				if (DepthBufferFormat::None != params.depthBufferFormat)
				{
					m->presentParams.AutoDepthStencilFormat = ToD3D9(params.depthBufferFormat);
					m->presentParams.EnableAutoDepthStencil = true;
				}
				m->presentParams.BackBufferFormat = ToD3D9(params.backBufferFormat);

				// Initialize the window
				if (pWindowManager->Initialize(
					params.hInstance
					, params.winProc
					, params.clientDimensions
					, params.windowMode
					, params.cmdLine
					, params.cmdShow
					, params.windowID
					, params.windowTitle
					, this))
				{
					// Initialize vsync settings
					m->hWnd = pWindowManager->GetWindowHandle();
					if (pVsyncManager->Initialize(params.vsync, this))
					{
						// Initialize anti-aliasing settings
						if (pAntiAliasManager->Initialize(params.antiAlias, this, pWindowManager))
						{
							// Initialize the Device
							if (m->InitDevice())
							{
								bSuccess = true;
							}
						}
					}
				}
			}
		}

		return bSuccess;
	}

	DeviceManager::DeviceManager()
		: m(make_unique<Impl>())
	{
	}
	DeviceManager::~DeviceManager()
	{
	}
}