#include "ScreenshotManager.h"

#include "DeviceManager.h"
#include "Resolution.h"
#include "WindowManager.h"

#include <Time/DeltaTimer.h>

#include <d3d9.h>
#include <d3dx9.h>

namespace gas
{
	struct ScreenshotManager::Impl
	{
		typedef void(*SurfaceDeleter)(IDirect3DSurface9*);

		static bool s_bInstantiated;

		DeltaTimer timer;

		string			dir;
		DeviceManager*	pDeviceManager;
		WindowManager*	pWindowManager;

		uptr<IDirect3DSurface9, SurfaceDeleter>	pFrontBuffer;
		uptr<IDirect3DSurface9, SurfaceDeleter>	pOffscreenSurface;

		SYSTEMTIME		systemTime;
		real			timeUntilNextShot;
		real			timeBetweenShots;
		string			savePath;

		Impl()
			: pDeviceManager(nullptr)
			, pWindowManager(nullptr)
			, pFrontBuffer(nullptr, SafeRelease<IDirect3DSurface9>)
			, pOffscreenSurface(nullptr, SafeRelease<IDirect3DSurface9>)
			, timeUntilNextShot(0)
			, timeBetweenShots(1)
		{
			assert(!s_bInstantiated && "ScreenshotManager::Initialize -- Attempted to create more than one ScreenshotManager instance.");
			s_bInstantiated = true;

			savePath.reserve(64);
		}
		~Impl()
		{
			s_bInstantiated = false;
		}
	};
	bool ScreenshotManager::Impl::s_bInstantiated = false;

	const string& ScreenshotManager::GetScreenshotDir() const
	{
		return m->dir;
	}
	void ScreenshotManager::SetScreenshotDir(const string& dir)
	{
		assert(dir.size() && "ScreenshotManager::SetScreenshotDirectory -- Invalid directory.");
		if (dir.size())
		{
			m->dir = dir;
		}
	}
	void ScreenshotManager::TakeScreenshot(const ImageType imgType /*= ImageType::Default*/)
	{
		// Determine if enough time has elapsed since the last shot
		if (m->timeUntilNextShot >= 0.0f)
		{
			m->timeUntilNextShot -= m->timer.Tick();
		}

		if (m->timeUntilNextShot <= 0.0f)
		{
			HRESULT hr;

			// Create the save path
			GetLocalTime(&m->systemTime);

			m->savePath = m->dir;
			m->savePath += std::to_string((int)m->systemTime.wYear) + "_";
			m->savePath += std::to_string((int)m->systemTime.wMonth) + "_";
			m->savePath += std::to_string((int)m->systemTime.wDay) + "_";
			m->savePath += std::to_string((int)m->systemTime.wHour) + "_";
			m->savePath += std::to_string((int)m->systemTime.wMinute) + "_";
			m->savePath += std::to_string((int)m->systemTime.wSecond);

			D3DXIMAGE_FILEFORMAT format = D3DXIFF_PNG;
			switch (imgType)
			{
			case ImageType::BMP: { m->savePath += ".bmp"; format = D3DXIFF_BMP; break; }
			case ImageType::DDS: { m->savePath += ".dds"; format = D3DXIFF_DDS; break; }
			case ImageType::JPG: { m->savePath += ".jpg"; format = D3DXIFF_JPG; break; }
			case ImageType::PNG: { m->savePath += ".png"; format = D3DXIFF_PNG; break; }
			case ImageType::TGA: { m->savePath += ".tga"; format = D3DXIFF_TGA; break; }
			}

			// Copy the buffer to the surface
			hr = m->pDeviceManager->GetDevice()->StretchRect(
				m->pFrontBuffer.get(),
				nullptr,
				m->pOffscreenSurface.get(),
				nullptr,
				D3DTEXF_NONE
				);

			// Save the screenshot
			hr = D3DXSaveSurfaceToFileA(
				m->savePath.c_str(),
				format,
				m->pFrontBuffer.get(),
				nullptr,
				nullptr
				);

			m->timeUntilNextShot = m->timeBetweenShots;
		}
	}

	bool ScreenshotManager::Initialize(const string& dir, const real delay, DeviceManager* pDeviceManager, WindowManager* pWindowManager)
	{
		bool bSuccess = false;

		assert(dir.size() && "ScreenshotManager::Initialize -- Invalid directory");
		assert(delay >= 0.9999f && "ScreenshotManager::Initialize -- Screenshot delay is too short. Initializing to 1 second.");
		assert(pDeviceManager && "ScreenshotManager::Initialize -- Invalid Device Manager.");
		assert(pWindowManager && "ScreenshotManager::Initialize -- Invalid Window Manager.");

		if (dir.size() && pDeviceManager && pWindowManager)
		{
			m->dir = dir + "/";
			m->timeBetweenShots = delay >= 0.9999f ? delay : 1;
			m->pDeviceManager = pDeviceManager;
			m->pWindowManager = pWindowManager;

			// Create the screenshot folder
			CreateDirectoryA(dir.c_str(), nullptr);

			// Get the front buffer
			IDirect3DSurface9* pFrontBuffer = nullptr;
			HRESULT hr = pDeviceManager->GetDevice()->GetRenderTarget(0, &pFrontBuffer);
			if (D3D_OK == hr)
			{
				m->pFrontBuffer.reset(pFrontBuffer);

				// Create an offscreen surface
				IDirect3DSurface9* pOffscreenSurface = nullptr;
				hr = pDeviceManager->GetDevice()->CreateOffscreenPlainSurface(
					pWindowManager->GetClientDimensions().width,
					pWindowManager->GetClientDimensions().height,
					pDeviceManager->GetParams().BackBufferFormat,
					D3DPOOL_DEFAULT,
					&pOffscreenSurface,
					nullptr
					);

				assert(D3D_OK == hr && "ScreenshotManager::Initialize -- Failed to create offscreen surface.");
				if (D3D_OK == hr)
				{
					m->pOffscreenSurface.reset(pOffscreenSurface);
					bSuccess = true;
				}
			}
		}

		return bSuccess;
	}
	ScreenshotManager::ScreenshotManager()
		: m(make_unique<Impl>())
	{
	}
	ScreenshotManager::~ScreenshotManager()
	{
	}
}