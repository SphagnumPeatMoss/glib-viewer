#pragma once
#define NOMINMAX

/***** Includes *****/
#include <Windows.h>
#include <WindowsX.h>
#define _CRTDBG_MAP_ALLOC
#include <crtdbg.h>
#include <cassert>

#include "config.h"
#include <list>
#include <map>
#include <memory>
#include <string>
#include <tchar.h>
#include <vector>

/***** Defines *****/
#define PURE = 0

namespace gas
{
	/***** typedefs *****/
	typedef float real;
	typedef unsigned int uint;
	typedef unsigned char ubyte;
	
	// typedef unique_ptr<T> uptr<T>
	template <typename T, typename Deleter = std::default_delete<T>> using uptr = std::unique_ptr<T, Deleter>;
	// typedef shared_ptr<T> sptr<T>
	template <typename T> using sptr = std::shared_ptr<T>;
	// typedef weak_ptr<T> wptr<T>
	template <typename T> using wptr = std::weak_ptr<T>;

	/***** usings *****/
	using std::make_unique;
	using std::make_shared;
	using std::list;
	using std::map;
	using std::pair;
	using std::string;
	using std::vector;

	/***** Statuses *****/
	const real EPSILON = (real)0.00001;
	enum class Status
	{
		// Right-most bit:	1 == GOOD, 0 == BAD
		// All other bits signify applicable details

		// GOOD
		Success = 0x1,
		Safe_NotFound = 0x3,
		AlreadyLoaded = 0x5,

		// BAD
		InternalError = 0x2,
		NotFound = 0x4,
		LoadFailed = 0x6,
		Duplicate = 0x8,
		ExceptionalError = 0xA,
		UniformNotFound = 0xC,
		InvalidCall = 0xE,
		InvalidParams = 0x10,
		WrongType = 0x12,
		UnknownFailure = 0
	};

	/***** Helpers *****/
	template <typename T> void SafeRelease(T* ptr)
	{
		if (ptr)
			ptr->Release();
	}
	template <typename T> void Delete(T* ptr)
	{
		delete ptr;
	}
	GOLDEN_API bool IsGood(const Status status);
}